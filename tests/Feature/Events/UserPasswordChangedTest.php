<?php

namespace Tests\Feature\Events;

use App\Events\UserPasswordChanged;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class UserPasswordChangedTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @return void
     */
    public function test_mail_notification_sent(): void
    {
        Notification::fake();

        $user = User::factory()->create();
        UserPasswordChanged::dispatch($user);

        Notification::assertSentTo($user, \App\Notifications\UserPasswordChanged::class);
    }
}
