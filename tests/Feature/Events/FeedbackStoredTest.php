<?php

namespace Tests\Feature\Events;

use App\Events\FeedbackStored;
use App\Listeners\SendFeedbackStoredNotification;
use App\Models\Feedback;
use Illuminate\Events\CallQueuedListener;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class FeedbackStoredTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @return void
     */
    public function test_queued_feedback_stored_notification(): void
    {
        Queue::fake();

        $feedback = Feedback::factory()->create();
        FeedbackStored::dispatch($feedback);

        Queue::assertPushed(CallQueuedListener::class, function ($job) use ($feedback) {
            return $job->class == SendFeedbackStoredNotification::class;
        });
    }
}
