<?php

namespace Tests\Feature\Events;

use App\Events\UserLoggedIn;
use App\Models\User;
use App\Notifications\NewUserLogin;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class UserLoggedTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @return void
     */
    public function test_mail_notification_sent(): void
    {
        Notification::fake();

        $user = User::factory()->create();
        UserLoggedIn::dispatch($user);

        Notification::assertSentTo($user, NewUserLogin::class);
    }
}
