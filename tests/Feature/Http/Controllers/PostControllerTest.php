<?php

namespace Tests\Feature\Http\Controllers;

use App\Events\PostStored;
use App\Http\Resources\PostPublishedPreviewCollection;
use App\Models\Post;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @return void
     */
    public function test_fetch_published_list_pagination(): void
    {
        $postsCount = 12;
        $posts = Post::factory()->count($postsCount)->create();

        $posts = $posts->sortByDesc('published_at');

        $expectedPagination = 5;
        $chunks = $posts->chunk($expectedPagination);

        $request = Request::create(route('posts.get_published_list'));

        foreach ($chunks as $page => $chunkPosts) {
            $page++;

            $response = $this->get(route('posts.get_published_list', [
                'page' => $page,
            ]));

            $response->assertOk();

            $response->assertJson(fn(AssertableJson $json) => $json
                ->has('data', count($chunkPosts))
                ->has('links')
                ->has('meta')
            );

            $chunkPostsFormatted = [];
            foreach ($chunkPosts as $item) {
                $chunkPostsFormatted[] = $item;
            }
            $postResource = new PostPublishedPreviewCollection($chunkPostsFormatted);
            $expectedPostChunks = $postResource->response($request)->getData(true);

            $response->assertJsonPath('data', $expectedPostChunks[PostPublishedPreviewCollection::DATA_KEY]);
        }
    }

    /**
     * @return void
     */
    public function test_fetch_only_published_posts(): void
    {
        $postsCount = 21;
        $posts = Post::factory()->count($postsCount)
            ->state(new Sequence(
                [
                    'published_at' => $this->faker->dateTime,
                ],
                [
                    'published_at' => null,
                ],
            ))
            ->create();

        $publishedPostCount = $posts->filter(function ($item) {
            if ($item->published_at) {
                return $item;
            }
        })->count();

        $this
            ->get(route('posts.get_published_list'))
            ->assertOk()
            ->assertJson(fn(AssertableJson $json) => $json
                ->has('data')
                ->has('links')
                ->has('meta')
                ->has('meta', fn($json) => $json
                    ->where('per_page', 5)
                    ->where('total', $publishedPostCount)
                    ->etc()
                )
            );
    }

    /**
     * @return void
     */
    public function test_non_veteran_user_invalid_post_create(): void
    {
        Event::fake();

        /** @var User $user */
        $user = User::factory()->create([
            'created_at' => Carbon::now(),
        ]);

        Sanctum::actingAs($user);

        $this
            ->postJson(route('posts.store_post'), [
                'name' => $this->faker->text(50),
                'description' => $this->faker->text(100),
                'text' => $this->faker->text(500),
            ])
            ->assertForbidden();

        Event::assertNotDispatched(PostStored::class);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function test_veteran_user_valid_post_create(): void
    {
        Event::fake();

        /** @var User $user */
        $user = User::factory()->create([
            'created_at' => Carbon::now()->subMonth()->subDay()
        ]);

        Sanctum::actingAs($user);

        $response = $this
            ->postJson(route('posts.store_post'), [
                'name' => $this->faker->text(50),
                'description' => $this->faker->text(100),
                'text' => $this->faker->text(500),
            ])
            ->assertOk()
            ->assertJson(fn(AssertableJson $json) => $json
                ->has('id')
                ->whereType('id', 'integer')
            );

        $responseData = $response->json();
        $postId = $responseData['id'];

        $post = Post::find($postId);
        $this->assertModelExists($post);
        $this->assertEquals($user->id, $post->author_id);

        Event::assertDispatched(PostStored::class);
    }
}
