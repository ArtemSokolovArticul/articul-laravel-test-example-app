<?php

namespace Tests\Feature\Http\Controllers;

use App\Events\UserLoggedIn;
use App\Events\UserLoggedOut;
use App\Listeners\SendUserNewLoginNotification;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @return void
     */
    public function test_invalid_credentials_auth(): void
    {
        Event::fake();

        $email = 'user@email.test';
        $password = 'foo';

        $user = User::factory()->create([
            'email' => $email,
            'password' => Hash::make($password),
        ]);

        $this->assertModelExists($user);

        $password = 'bar';

        $this
            ->postJson(route('login'), [
                'email' => $email,
                'password' => $password,
            ])
            ->assertForbidden()
            ->assertJsonStructure([
                'message',
            ]);

        Event::assertNotDispatched(UserLoggedIn::class);
    }

    /**
     * @return void
     */
    public function test_valid_credentials_auth(): void
    {
        Event::fake();

        $email = 'user@email.test';
        $password = 'foo';

        $user = User::factory()->create([
            'email' => $email,
            'password' => Hash::make($password),
        ]);

        $this->assertModelExists($user);

        $this
            ->postJson(route('login'), [
                'email' => $email,
                'password' => $password,
            ])
            ->assertOk()
            ->assertJsonStructure([
                'token',
            ]);

        Event::assertDispatched(UserLoggedIn::class);
        Event::assertListening(
            UserLoggedIn::class,
            SendUserNewLoginNotification::class
        );
    }

    /**
     * @return void
     */
    public function test_valid_auth_logout(): void
    {
        Event::fake();

        $user = User::factory()->create();

        Sanctum::actingAs($user);

        $this
            ->postJson(route('logout'))
            ->assertOk()
            ->assertJsonStructure([
                'success',
            ]);

        $user->refresh();

        Event::assertDispatched(UserLoggedOut::class);
    }

    /**
     * @return void
     */
    public function test_invalid_anonymous_logout(): void
    {
        Event::fake();

        $this
            ->postJson(route('logout'))
            ->assertUnauthorized();

        Event::assertNotDispatched(UserLoggedOut::class);
    }
}
