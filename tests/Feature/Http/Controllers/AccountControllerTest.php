<?php

namespace Tests\Feature\Http\Controllers;

use App\Events\UserPasswordChanged;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AccountControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @return void
     */
    public function test_change_password(): void
    {
        Event::fake();

        $firstPassword = 'foo123';

        /** @var User $user */
        $user = User::factory()->create([
            'password' => Hash::make($firstPassword),
        ]);

        Sanctum::actingAs($user);

        $secondPassword = 'bar456';

        $this
            ->putJson(route('account.change_password'), [
                'password' => $secondPassword,
            ])
            ->assertOk()
            ->assertJson(fn(AssertableJson $json) => $json
                ->where('success', true)
            );

        $user->refresh();

        $this->assertTrue(Hash::check($secondPassword, $user->password));

        Event::assertDispatched(UserPasswordChanged::class);
    }

    /**
     * @return void
     */
    public function test_get_account_info(): void
    {
        Event::fake();

        /** @var User $user */
        $user = User::factory()->create();

        Sanctum::actingAs($user);

        $this
            ->get(route('account.get_info'))
            ->assertOk()
            ->assertJson(fn(AssertableJson $json) => $json
                ->has('info', fn($json) => $json
                    ->where('name', $user->name)
                    ->where('email', $user->email)
                    ->missing('password')
                )
            );
    }

    /**
     * @return void
     */
    public function test_revoke_all_sessions(): void
    {
        Event::fake();

        /** @var User $user */
        $user = User::factory()->create();
        $tokensCount = 5;
        for ($i = 1; $i <= $tokensCount; $i++) {
            $user->createToken(sprintf("Test API token %s", $i));
        }

        $user->refresh();

        $this->assertCount($tokensCount, $user->tokens);

        Sanctum::actingAs($user);

        $this
            ->postJson(route('account.revoke_all_sessions'))
            ->assertOk()
            ->assertJsonStructure([
                'success',
            ]);

        $user->refresh();

        $this->assertCount(0, $user->tokens);
    }
}
