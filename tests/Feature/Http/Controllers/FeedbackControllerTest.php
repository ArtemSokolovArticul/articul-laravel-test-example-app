<?php

namespace Tests\Feature\Http\Controllers;

use App\Events\FeedbackStored;
use App\Listeners\SendFeedbackStoredNotification;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Exception;

class FeedbackControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @return void
     * @throws Exception
     */
    public function test_valid_submit_feedback_authorized(): void
    {
        Event::fake();

        $text = 'My authorized feedback text';

        /** @var User $user */
        $user = User::factory()->create();

        $this
            ->actingAs($user)
            ->postJson(route('feedback.submit'), [
                'text' => $text,
            ])
            ->assertOk()
            ->assertJsonStructure([
                'number'
            ]);

        Event::assertDispatched(FeedbackStored::class);
        Event::assertListening(
            FeedbackStored::class,
            SendFeedbackStoredNotification::class
        );
    }

    /**
     * @return void
     */
    public function test_invalid_submit_feedback_authorized(): void
    {
        Event::fake();

        /** @var User $user */
        $user = User::factory()->create();

        $this
            ->actingAs($user)
            ->postJson(route('feedback.submit'))
            ->assertUnprocessable()
            ->assertJsonValidationErrors([
                'text',
            ]);

        Event::assertNotDispatched(FeedbackStored::class);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function test_valid_submit_feedback_anonymous(): void
    {
        Event::fake();

        $email = 'user@email.test';
        $text = 'My anonymous feedback text';

        $this
            ->postJson(route('feedback.submit'), [
                'email' => $email,
                'text' => $text,
            ])
            ->assertOk()
            ->assertJsonStructure([
                'number'
            ]);

        Event::assertDispatched(FeedbackStored::class);
        Event::assertListening(
            FeedbackStored::class,
            SendFeedbackStoredNotification::class
        );
    }

    /**
     * @return void
     */
    public function test_invalid_submit_feedback_anonymous(): void
    {
        Event::fake();

        $this
            ->postJson(route('feedback.submit'))
            ->assertUnprocessable()
            ->assertJsonValidationErrors([
                'email',
                'text',
            ]);

        Event::assertNotDispatched(FeedbackStored::class);
    }
}
