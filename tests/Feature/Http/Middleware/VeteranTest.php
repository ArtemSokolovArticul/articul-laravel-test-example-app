<?php

namespace Tests\Feature\Http\Middleware;

use App\Http\Middleware\Veteran;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\TestCase;

class VeteranTest extends TestCase
{
    /**
     * @return void
     */
    public function test_non_authorized_user_request(): void
    {
        $veteranMiddleware = new Veteran();

        try {
            $veteranMiddleware->handle(
                new Request(),
                fn() => new Response()
            );
        } catch (HttpException $e) {
            $this->assertEquals(Response::HTTP_UNAUTHORIZED, $e->getStatusCode());
        }
    }

    /**
     * @return void
     */
    public function test_valid_user_request(): void
    {
        $this->withoutExceptionHandling();

        /** @var User $user */
        $user = User::factory()->make([
            'created_at' => Carbon::now()->subMonths(2),
        ]);

        $this->be($user);

        $veteranMiddleware = new Veteran();

        /** @var Response $response */
        $response = $veteranMiddleware->handle(
            new Request(),
            fn() => new Response()
        );

        $this->assertTrue($response->isOk());
    }

    /**
     * @return void
     */
    public function test_invalid_user_request(): void
    {
        /** @var User $user */
        $user = User::factory()->make([
            'created_at' => Carbon::now()->subWeek(),
        ]);

        $this->be($user);

        $veteranMiddleware = new Veteran();

        try {
            $veteranMiddleware->handle(
                new Request(),
                fn() => new Response()
            );
        } catch (HttpException $e) {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $e->getStatusCode());
        }
    }
}
