<?php

namespace Tests\Feature\Services;

use App\Data\FeedbackData;
use App\Models\User;
use App\Services\FeedbackService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

class FeedbackServiceTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @return void
     */
    public function test_store_feedback_authorized(): void
    {
        /** @var User $user */
        $user = User::factory()->create();
        $text = 'Feedback text';
        $email = 'feedback_email@email.com';

        $feedbackData = FeedbackData::from([
            'text' => $text,
            'email' => $email,
            'user' => $user,
        ]);

        /** @var FeedbackService $feedbackService */
        $feedbackService = App::make(FeedbackService::class);
        $feedback = $feedbackService->storeFeedback($feedbackData);

        $this->assertModelExists($feedback);
        $this->assertEquals($feedbackData->text, $feedback->text);
        $this->assertEquals($feedbackData->email, $feedback->email);
        $this->assertEquals($feedbackData->user->id, $feedback->user_id);
    }

    /**
     * @return void
     */
    public function test_store_feedback_anonymous(): void
    {
        $text = 'Feedback text';
        $email = 'feedback_email@email.com';

        $feedbackData = FeedbackData::from([
            'text' => $text,
            'email' => $email,
        ]);

        /** @var FeedbackService $feedbackService */
        $feedbackService = App::make(FeedbackService::class);
        $feedback = $feedbackService->storeFeedback($feedbackData);

        $this->assertModelExists($feedback);
        $this->assertEquals($feedbackData->text, $feedback->text);
        $this->assertEquals($feedbackData->email, $feedback->email);
        $this->assertNull($feedback->user_id);
    }
}
