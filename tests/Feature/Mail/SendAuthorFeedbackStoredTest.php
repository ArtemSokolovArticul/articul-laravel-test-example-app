<?php

namespace Tests\Feature\Mail;

use App\Mail\SendAuthorFeedbackStored;
use App\Models\Feedback;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class SendAuthorFeedbackStoredTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @return void
     */
    public function test_correct_mail_content(): void
    {
        Queue::fake();

        $feedback = Feedback::factory()->create();

        $mailable = new SendAuthorFeedbackStored($feedback);

        $mailable->assertSeeInHtml(sprintf("Feedback %s stored!", $feedback->id));
    }
}
