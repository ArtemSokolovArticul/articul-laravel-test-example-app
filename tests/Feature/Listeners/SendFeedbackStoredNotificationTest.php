<?php

namespace Tests\Feature\Listeners;

use App\Events\FeedbackStored;
use App\Listeners\SendFeedbackStoredNotification;
use App\Mail\SendAdminFeedbackStored;
use App\Mail\SendAuthorFeedbackStored;
use App\Models\Feedback;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class SendFeedbackStoredNotificationTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @return void
     */
    public function test_mail_sent_anonymous(): void
    {
        Mail::fake();

        $feedback = Feedback::factory()->create();
        $event = new FeedbackStored($feedback);
        $listener = new SendFeedbackStoredNotification();
        $listener->handle($event);

        Mail::assertSent(SendAuthorFeedbackStored::class, function ($mail) use ($feedback) {
            return $mail->hasTo($feedback->email);
        });
        Mail::assertSent(SendAdminFeedbackStored::class, function ($mail) {
            return $mail->hasTo(env('ADMIN_EMAIL'));
        });
    }

    /**
     * @return void
     */
    public function test_mail_sent_authorized(): void
    {
        Mail::fake();

        $user = User::factory()->create();
        $feedback = Feedback::factory()->create([
            'user_id' => $user->id,
        ]);
        $event = new FeedbackStored($feedback);
        $listener = new SendFeedbackStoredNotification();
        $listener->handle($event);

        Mail::assertSent(SendAuthorFeedbackStored::class, function ($mail) use ($user) {
            return $mail->hasTo($user->email);
        });
        Mail::assertSent(SendAdminFeedbackStored::class, function ($mail) {
            return $mail->hasTo(env('ADMIN_EMAIL'));
        });
    }
}
