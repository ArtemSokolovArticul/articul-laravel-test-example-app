<?php

namespace Tests\Feature;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

class UserServiceTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @return void
     */
    public function test_create_token(): void
    {
        /** @var UserService $userService */
        $userService = App::make(UserService::class);

        /** @var User $user */
        $user = User::factory()->create();

        $tokenName = 'Test API token';
        $tokenString = $userService->generateTokenString($user, $tokenName);

        $user->refresh();

        $this->assertCount(1, $user->tokens);
        $token = $user->tokens->first();

        $this->assertEquals($tokenName, $token->name);
        $this->assertEquals(hash('sha256', $tokenString), $token->token);
    }
}
