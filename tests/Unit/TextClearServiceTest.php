<?php

namespace Tests\Unit;

use App\Services\TextClearService;
use PHPUnit\Framework\TestCase;

class TextClearServiceTest extends TestCase
{
    /**
     * @return void
     */
    public function test_clear_tags(): void
    {
        $string = '<div>Съешь еще этих мягких французских <b>булок</b>.</div>';

        $clearString = TextClearService::clearTags($string);

        $this->assertEquals('Съешь еще этих мягких французских булок.', $clearString);
    }

    /**
     * @return void
     */
    public function test_clear_emoji(): void
    {
        $string = '🤬Съешь 💋еще 😴этих 💩мягких 🤑французских 🤪булок.🤡';

        $clearString = TextClearService::clearEmoji($string);

        $this->assertEquals('Съешь еще этих мягких французских булок.', $clearString);
    }

    /**
     * @return void
     */
    public function test_clear(): void
    {
        $string = '<div>🤬Съешь 💋еще 😴этих 💩мягких 🤑французских 🤪булок.🤡</div>';

        $clearString = TextClearService::clear($string);

        // todo: mock внутренних методов

        $this->assertEquals('Съешь еще этих мягких французских булок.', $clearString);
    }
}
