<?php

namespace App\Helpers;

class ValidationHelper
{
    /**
     * @return string[]
     */
    public static function getPasswordRules(): array
    {
        return [
            'required',
            'string',
            'min:6',
            'max:36'
        ];
    }
}
