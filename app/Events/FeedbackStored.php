<?php

namespace App\Events;

use App\Models\Feedback;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class FeedbackStored
{
    use Dispatchable, SerializesModels;

    public function __construct(
        public Feedback $feedback
    )
    {
        //
    }
}
