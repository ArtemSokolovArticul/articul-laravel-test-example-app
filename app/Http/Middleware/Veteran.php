<?php

namespace App\Http\Middleware;

use App\Models\User;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class Veteran
{
    public function handle(Request $request, Closure $next)
    {
        if (!Auth::check()) {
            abort(Response::HTTP_UNAUTHORIZED);
        }

        /** @var User $user */
        $user = Auth::user();
        $date = Carbon::now()->subMonth();

        if ($user->created_at >= $date) {
            abort(Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
