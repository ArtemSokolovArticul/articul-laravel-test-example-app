<?php

namespace App\Http\Controllers\Api;

use App\Data\Requests\AuthLoginRequestData;
use App\Data\Requests\AuthRegisterRequestData;
use App\Events\UserLoggedOut;
use App\Events\UserRegistered;
use App\Models\User;
use App\Events\UserLoggedIn;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct(
        private UserService $userService
    )
    {
    }

    /**
     * @param AuthRegisterRequestData $data
     * @return JsonResponse
     */
    public function register(AuthRegisterRequestData $data): JsonResponse
    {
        $user = User::create([
            'email' => $data->email,
            'name' => $data->name,
            'password' => Hash::make($data->password)
        ]);

        UserRegistered::dispatch($user);

        return response()->json([
            'id' => $user->id,
        ]);
    }

    /**
     * @param AuthLoginRequestData $data
     * @return JsonResponse
     */
    public function login(AuthLoginRequestData $data): JsonResponse
    {
        if (!Auth::attempt([
            'email' => $data->email,
            'password' => $data->password
        ])) {
            return response()->json([
                'message' => __('Invalid user credentials.'),
            ], Response::HTTP_FORBIDDEN);
        }

        /** @var User $user */
        $user = User::where('email', $data->email)->first();

        UserLoggedIn::dispatch($user);

        return response()->json([
            'token' => $this->userService->generateTokenString($user)
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();
        $user->currentAccessToken()->delete();

        UserLoggedOut::dispatch($user);

        return response()->json([
            'success' => true,
        ]);
    }
}
