<?php

namespace App\Http\Controllers\Api;

use App\Data\Requests\PostStoreRequestData;
use App\Events\PostStored;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostPublishedPreviewCollection;
use App\Models\Post;
use App\Models\User;
use App\Services\TextClearService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * @return PostPublishedPreviewCollection
     */
    public function getPublishedList(): PostPublishedPreviewCollection
    {
        $posts = Post::active()->orderBy('published_at', 'desc')->paginate(5);
        return new PostPublishedPreviewCollection($posts);
    }

    /**
     * @param PostStoreRequestData $data
     * @return array
     */
    public function storePost(PostStoreRequestData $data): array
    {
        /** @var User $user */
        $user = Auth::user();
        $now = Carbon::now();

        $post = $user->posts()->create([
            'name' => TextClearService::clear($data->name),
            'description' => $data->description,
            'text' => $data->text,
            'published_at' => $now,
        ]);

        PostStored::dispatch($post);

        return [
            'id' => $post->id,
        ];
    }
}
