<?php

namespace App\Http\Controllers\Api;

use App\Data\FeedbackData;
use App\Data\Requests\FeedbackSubmitRequestData;
use App\Events\FeedbackStored;
use App\Http\Controllers\Controller;
use App\Services\FeedbackService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class FeedbackController extends Controller
{
    public function __construct(
        private FeedbackService $feedbackService,
    )
    {
    }

    /**
     * @param FeedbackSubmitRequestData $data
     * @return JsonResponse
     */
    public function submitFeedback(FeedbackSubmitRequestData $data): JsonResponse
    {
        $feedback = $this->feedbackService->storeFeedback(FeedbackData::from([
            'email' => $data->email ?? null,
            'text' => $data->text ?? null,
            'user' => Auth::user() ?? null,
        ]));

        FeedbackStored::dispatch($feedback);

        return response()->json([
            'number' => $feedback->id,
        ]);
    }
}
