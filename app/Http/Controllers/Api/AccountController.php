<?php

namespace App\Http\Controllers\Api;

use App\Data\Requests\AccountChangePasswordRequestData;
use App\Events\UserPasswordChanged;
use App\Http\Controllers\Controller;
use App\Http\Resources\AccountInfo;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    /**
     * @param AccountChangePasswordRequestData $data
     * @return JsonResponse
     */
    public function changePassword(AccountChangePasswordRequestData $data): JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();

        $user->password = Hash::make($data->password);
        $user->save();

        UserPasswordChanged::dispatch($user->refresh());

        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function revokeAllSessions(): JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();

        $user->tokens()->delete();

        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * @return AccountInfo
     */
    public function getInfo(): AccountInfo
    {
        /** @var User $user */
        $user = Auth::user();

        return new AccountInfo($user);
    }
}
