<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PostPublishedPreviewCollection extends ResourceCollection
{
    public $collects = PostPublishedPreview::class;

    public const DATA_KEY = 'data';

    public function toArray($request): array
    {
        return [
            static::DATA_KEY => $this->collection,
        ];
    }
}
