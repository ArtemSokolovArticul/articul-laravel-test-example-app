<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostPublishedPreview extends JsonResource
{
    public static $wrap = 'post';

    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'published_at' => $this->published_at,
            'author' => $this->whenLoaded('author', new PostAuthor($this->author))
        ];
    }
}
