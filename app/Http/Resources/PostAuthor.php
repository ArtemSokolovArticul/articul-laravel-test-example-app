<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostAuthor extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'name' => $this->name,
            'registered_at' => $this->created_at,
        ];
    }
}
