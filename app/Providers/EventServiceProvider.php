<?php

namespace App\Providers;

use App\Events\FeedbackStored;
use App\Events\PostStored;
use App\Events\UserLoggedIn;
use App\Events\UserLoggedOut;
use App\Events\UserPasswordChanged;
use App\Events\UserRegistered;
use App\Listeners\SendFeedbackStoredNotification;
use App\Listeners\SendUserNewLoginNotification;
use App\Listeners\SendUserPasswordChangedNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        UserRegistered::class => [
            //
        ],
        UserLoggedIn::class => [
            SendUserNewLoginNotification::class,
        ],
        UserLoggedOut::class => [
            //
        ],
        UserPasswordChanged::class => [
            SendUserPasswordChangedNotification::class,
        ],
        FeedbackStored::class => [
            SendFeedbackStoredNotification::class
        ],
        PostStored::class => [
            //
        ],
    ];

    public function boot()
    {
        //
    }

    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
