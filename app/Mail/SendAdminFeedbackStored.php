<?php

namespace App\Mail;

use App\Models\Feedback;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendAdminFeedbackStored extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public Feedback $feedback
    )
    {
    }

    public function build(): SendAdminFeedbackStored
    {
        return $this
            ->subject(__('Feedback stored'))
            ->view('email.feedback.admin_feedback_stored');
    }
}
