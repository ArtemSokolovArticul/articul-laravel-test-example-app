<?php

namespace App\Mail;

use App\Models\Feedback;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendAuthorFeedbackStored extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public Feedback $feedback
    )
    {
    }

    public function build(): SendAuthorFeedbackStored
    {
        return $this
            ->subject(__('Your feedback stored'))
            ->view('email.feedback.author_feedback_stored');
    }
}
