<?php

namespace App\Services;

use App\Data\FeedbackData;
use App\Models\Feedback;
use Illuminate\Database\Eloquent\Model;

class FeedbackService
{
    /**
     * @param FeedbackData $feedbackData
     * @return Model
     */
    public function storeFeedback(FeedbackData $feedbackData): Model
    {
        return Feedback::create([
            'text' => $feedbackData->text,
            'email' => $feedbackData->email,
            'user_id' => $feedbackData->user?->id,
        ]);
    }
}
