<?php

namespace App\Services;

use App\Models\User;

class UserService
{
    /**
     * @param User $user
     * @param string $name
     * @return string
     */
    public function generateTokenString(User $user, string $name = 'API Token'): string
    {
        $tokenParts = explode('|', $user->createToken($name)->plainTextToken);
        return $tokenParts[1];
    }
}
