<?php

namespace App\Listeners;

use App\Events\FeedbackStored;
use App\Mail\SendAdminFeedbackStored;
use App\Mail\SendAuthorFeedbackStored;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendFeedbackStoredNotification implements ShouldQueue
{
    public function __construct()
    {
        //
    }

    /**
     * @param FeedbackStored $event
     * @return void
     */
    public function handle(FeedbackStored $event): void
    {
        $feedback = $event->feedback;

        // author send
        $to = $feedback->user ?: $feedback->email;
        Mail::to($to)->send(new SendAuthorFeedbackStored($feedback));

        // admin send
        Mail::to(env('ADMIN_EMAIL'))->send(new SendAdminFeedbackStored($feedback));
    }
}
