<?php

namespace App\Listeners;

use App\Events\UserLoggedIn;
use App\Notifications\NewUserLogin;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserNewLoginNotification implements ShouldQueue
{
    public function __construct()
    {
        //
    }

    /**
     * @param UserLoggedIn $event
     * @return void
     */
    public function handle(UserLoggedIn $event): void
    {
        $user = $event->user;
        $user->notify(new NewUserLogin());
    }
}
