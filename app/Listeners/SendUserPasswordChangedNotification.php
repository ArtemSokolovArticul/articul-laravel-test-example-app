<?php

namespace App\Listeners;

use App\Notifications\UserPasswordChanged;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserPasswordChangedNotification implements ShouldQueue
{
    public function __construct()
    {
        //
    }

    /**
     * @param \App\Events\UserPasswordChanged $event
     * @return void
     */
    public function handle(\App\Events\UserPasswordChanged $event): void
    {
        $user = $event->user;
        $user->notify(new UserPasswordChanged());
    }
}
