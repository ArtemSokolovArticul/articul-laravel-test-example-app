<?php

namespace App\Data;

use App\Models\User;
use Spatie\LaravelData\Data;

class FeedbackData extends Data
{
    public function __construct(
        public ?string $text = null,
        public ?string $email = null,
        public ?User   $user = null,
    )
    {
    }
}
