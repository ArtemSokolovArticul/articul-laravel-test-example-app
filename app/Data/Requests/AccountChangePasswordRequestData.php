<?php

namespace App\Data\Requests;

use Spatie\LaravelData\Data;

class AccountChangePasswordRequestData extends Data
{
    public function __construct(
        public string $password
    )
    {
    }

    public static function rules(): array
    {
        return [
            'password' => [
                'required'
            ]
        ];
    }
}
