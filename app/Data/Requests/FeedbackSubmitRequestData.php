<?php

namespace App\Data\Requests;

use Illuminate\Support\Facades\Auth;
use Spatie\LaravelData\Data;

class FeedbackSubmitRequestData extends Data
{
    public function __construct(
        public ?string $email,
        public string $text,
    )
    {
    }

    public static function rules(): array
    {
        return [
            'email' => [
                Auth::check() ? 'sometimes' : 'required',
                'email',
            ],
            'text' => [
                'required',
                'string',
                'min:10',
                'max:3000',
            ],
        ];
    }
}
