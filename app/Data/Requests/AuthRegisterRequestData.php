<?php

namespace App\Data\Requests;

use App\Helpers\ValidationHelper;
use Spatie\LaravelData\Data;

class AuthRegisterRequestData extends Data
{
    public function __construct(
        public string $name,
        public string $email,
        public string $password
    )
    {
    }

    public static function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'max:200',
            ],
            'email' => [
                'required',
                'email',
                sprintf("unique:%s,email", 'users')
            ],
            'password' => ValidationHelper::getPasswordRules()
        ];
    }
}
