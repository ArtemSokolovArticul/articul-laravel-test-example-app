<?php

namespace App\Data\Requests;

use Spatie\LaravelData\Data;

class AuthLoginRequestData extends Data
{
    public function __construct(
        public string $email,
        public string $password
    )
    {
    }

    public static function rules(): array
    {
        return [
            'email' => [
                'required',
                'email',
            ],
            'password' => [
                'required'
            ]
        ];
    }
}
