<?php

namespace App\Data\Requests;

use Spatie\LaravelData\Data;

class PostStoreRequestData extends Data
{
    public function __construct(
        public string $name,
        public ?string $description,
        public string $text,
    )
    {
    }

    public static function rules(): array
    {
        return [
            'name' => [
                'string',
                'min:6',
                'max:300',
            ],
            'description' => [
                'string',
                'min:6',
                'max:1000',
            ],
            'text' => [
                'string',
                'min:100',
                'max:5000',
            ],
        ];
    }
}
