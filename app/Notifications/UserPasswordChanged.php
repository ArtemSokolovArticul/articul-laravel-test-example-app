<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserPasswordChanged extends Notification implements ShouldQueue
{
    use Queueable;

    public function __construct()
    {
        //
    }

    public function via($notifiable): array
    {
        return [
            'mail'
        ];
    }

    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)->view('email.user.user_password_changed');
    }

    public function toArray($notifiable): array
    {
        return [
            //
        ];
    }
}
