<?php

use App\Http\Controllers\Api\AccountController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\FeedbackController;
use App\Http\Controllers\Api\PostController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->prefix('account')->group(function () {
    Route::get('/', [AccountController::class, 'getInfo'])
        ->name('account.get_info');

    Route::put('password', [AccountController::class, 'changePassword'])
        ->name('account.change_password');

    Route::post('revoke_all_sessions', [AccountController::class, 'revokeAllSessions'])
        ->name('account.revoke_all_sessions');
});

Route::post('feedback', [FeedbackController::class, 'submitFeedback'])
    ->name('feedback.submit');

Route::post('register', [AuthController::class, 'register'])
    ->name('register');

Route::post('login', [AuthController::class, 'login'])
    ->name('login');

Route::middleware('auth:sanctum')
    ->post('logout', [AuthController::class, 'logout'])
    ->name('logout');

Route::prefix('posts')->group(function () {
    Route::get('/', [PostController::class, 'getPublishedList'])
        ->name('posts.get_published_list');
    Route::middleware(['veteran'])
        ->post('/', [PostController::class, 'storePost'])
        ->name('posts.store_post');
});
