<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Feedback stored</title>
</head>
<body>
    <h1>Feedback {{ $feedback->id }} stored! Please, review it.</h1>
</body>
</html>
